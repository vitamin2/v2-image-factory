# v2-image-factory Helper

> Just an image factory
****

## Installation

### Download

Download and copy this repository to `/site/plugins/v2-image-factory`.

### Composer

```
composer require vitamin2/v2-image-factory
```

## Setup

1. Install plugin

2. Add Image Factory JS to your template (getFactoryJS returns a versionised JS file)

```php
  <?= js([
    getFactoryJs(),
  ]) ?>
```

3. Preload it (optional)

```php
  <link rel="preload" href="<?= getFactoryJs() ?>" as="script" />
```

## Usage

1. imageURL
  choose image

2. imageType
  choose crop type
  - variousHeights (choose this if you have differnt image heights, e.g for base image or logos)
  - cropToRatio (choose this if you have a fixed ratio e.g. for gallery, headers)
  - cropToRatioMultiple (choose this if you have 2 ratios, mobile and desktop)

3. imageRatio (for cropToRatio and cropToRatioMultiple)
  choose image ratio (e.g. 16/9 = 1.777 or 4/3 = 1.333)

4. imageRatioDesktop (for cropToRatioMultiple)
  choose image ratio for Desktop

5. imageCustomClass
  add a custom class here

6. imageCropToPoint (for cropToRatio, cropToRatioMultiple)
  select crop point
  - default is center, but you can also set left, top, etc.

```php
  snippet('global/image-factory', [
    'imageURL' => $data->gallery()->toFile(),
    'imageType' => 'variousHeights',
    'imageRatio' => '1.333',
    'imageRatioDesktop' => '3',
    'imageCustomClass' => '',
    'imageCropToPoint' => 'center'
  ])
```

## License

MIT
