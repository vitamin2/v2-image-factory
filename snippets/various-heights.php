<?php
$srcsetArr = getSrcSetArray($format);
snippet("factory/picture", [
    "format" => $format,
    "ratioHeight" => $image->height(),
    "ratioWidth" => $image->width(),
    "class" => $class,
    "image" => $image,
    "srcsetArr" => $srcsetArr,
]);
