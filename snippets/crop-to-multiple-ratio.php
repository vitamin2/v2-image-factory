<?php
$srcsetArr = getCropToRatioMultipleSrcSetArray($format, $imageRatio, $imageRatioDesktop, $imageCropToPoint);
$imageClass = uniqID("image-");
?>
<style>
    <?= getRatioMultipleCss($srcsetArr, $imageClass) ?>
</style>

<picture class="ratio-auto <?= $imageClass ?>">
    <!--[if IE 9]><video style="display: none"><![endif]-->
    <?php foreach ($srcsetArr as $src): ?>
        <source srcset="<?= $image->thumb($src)->url() ?>" media="(max-width: <?= $src["width"] ?>px)" />
    <?php endforeach; ?>
    <!--[if IE 9]></video><![endif]-->
    <img loading="lazy" class="<?= $class ?>"
        width="<?= $image->width() ?>" height="<?= $image->height() ?>" src="
        <?= $image
            ->thumb(["width" => 1000, "format" => $format])
            ->url() ?>" alt="<?= $image->alt() ?>" title="<?= $image->title() ?>
    ">
</picture>
