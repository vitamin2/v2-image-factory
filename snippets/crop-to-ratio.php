<?php
$srcsetArr = getSrcSetArray($format, $imageRatio, $imageCropToPoint);
snippet("factory/picture", [
    "format" => $format,
    "ratioHeight" => $srcsetArr["320w"]["height"],
    "ratioWidth" => $srcsetArr["320w"]["width"],
    "class" => $class,
    "image" => $image,
    "srcsetArr" => $srcsetArr,
]);
