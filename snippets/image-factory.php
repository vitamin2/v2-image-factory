<?php
$hasFile = false;
isset($imageURL) ? ($hasFile = true) : ($hasFile = false);
if ($hasFile):
    $format = getWebpSupport() ? $kirby->option("thumbs.format") : $imageURL->extension(); ?>
  <noscript class='loading-lazy'>

      <?php if ($imageType === "variousHeights") {
          snippet("factory/various-heights", [
              "format" => $format,
              "ratioHeight" => $imageURL->height(),
              "ratioWidth" => $imageURL->width(),
              "class" => $imageCustomClass,
              "image" => $imageURL,
          ]);
      } ?>
      
      <?php if ($imageType === "cropToRatio") {
          snippet("factory/crop-to-ratio", [
              "format" => $format,
              "imageRatio" => $imageRatio,
              "imageCropToPoint" => $imageCropToPoint,
              "class" => $imageCustomClass,
              "image" => $imageURL,
          ]);
      } ?>

      <?php if ($imageType === "cropToRatioMultiple") {
          snippet("factory/crop-to-multiple-ratio", [
              "format" => $format,
              "imageRatio" => $imageRatio,
              "imageRatioDesktop" => $imageRatioDesktop,
              "imageCropToPoint" => $imageCropToPoint,
              "class" => $imageCustomClass,
              "image" => $imageURL,
          ]);
      } ?>
  </noscript>
<?php
endif; ?>
