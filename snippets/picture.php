<picture class="ratio-auto" style="<?= getRatioCss($ratioHeight, $ratioWidth) ?>">
    <img loading="lazy" width="<?= $image->width() ?>px" height="<?= $image->height() ?>px" srcset="<?= $image->srcset(
    $srcsetArr
) ?>" src="<?= $image
    ->thumb(["width" => 1000, "format" => $format])
    ->url() ?>" alt="<?= $image->alt() ?>" title="<?= $image->title() ?>" sizes="
    <?= getMaxWidthString($srcsetArr) ?>" />
</picture>
