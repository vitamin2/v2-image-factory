<?php

Kirby::plugin("vitamin2/v2-image-factory", [
    "snippets" => [
        "global/image-factory" => __DIR__ . "/snippets/image-factory.php",
        "factory/picture" => __DIR__ . "/snippets/picture.php",
        "factory/various-heights" => __DIR__ . "/snippets/various-heights.php",
        "factory/crop-to-ratio" => __DIR__ . "/snippets/crop-to-ratio.php",
        "factory/crop-to-multiple-ratio" => __DIR__ . "/snippets/crop-to-multiple-ratio.php",
    ],
]);

function getFactoryJs(): string
{
    $pathToJs = "media/plugins/vitamin2/v2-image-factory/bundle.js";
    $mod = new Asset($pathToJs);
    $moddate = $mod->modified();
    return $pathToJs . "?v=" . $moddate;
}

function getWebpSupport(): bool
{
    $supportsWebP = false;
    if (isset($_SERVER["HTTP_ACCEPT"]) && stripos($_SERVER["HTTP_ACCEPT"], "image/webp") !== false) {
        $supportsWebP = true;
    }
    return $supportsWebP;
}

function getSrcSetArray($format, $imageRatio = null, $imageCropToPoint = null): array
{
    $srcset = [
        "320w" => [
            "width" => intval(320),
            "format" => $format,
        ],
        "420w" => [
            "width" => intval(420),
            "format" => $format,
        ],
        "768w" => [
            "width" => intval(768),
            "format" => $format,
        ],
        "1024w" => [
            "width" => intval(1024),
            "format" => $format,
        ],
        "1440w" => [
            "width" => intval(1440),
            "format" => $format,
        ],
        "2800w" => [
            "width" => intval(2800),
            "format" => $format,
        ],
    ];

    if ($imageRatio !== null && $imageCropToPoint !== null) {
        $srcset["320w"]["height"] = intval(320 / $imageRatio);
        $srcset["420w"]["height"] = intval(420 / $imageRatio);
        $srcset["768w"]["height"] = intval(768 / $imageRatio);
        $srcset["1024w"]["height"] = intval(1024 / $imageRatio);
        $srcset["1440w"]["height"] = intval(1440 / $imageRatio);
        $srcset["2800w"]["height"] = intval(2800 / $imageRatio);
        $srcset["320w"]["crop"] = $imageCropToPoint;
        $srcset["420w"]["crop"] = $imageCropToPoint;
        $srcset["768w"]["crop"] = $imageCropToPoint;
        $srcset["1024w"]["crop"] = $imageCropToPoint;
        $srcset["1440w"]["crop"] = $imageCropToPoint;
        $srcset["2800w"]["crop"] = $imageCropToPoint;
    }
    return $srcset;
}

function getCropToRatioMultipleSrcSetArray($format, $imageRatio, $imageRatioDesktop, $imageCropToPoint): array
{
    return [
        "1024w" => [
            "width" => intval(1024),
            "height" => intval(1024 / $imageRatio),
            "crop" => $imageCropToPoint,
            "format" => $format,
        ],
        "2800w" => [
            "width" => intval(2800),
            "height" => intval(2800 / $imageRatioDesktop),
            "crop" => $imageCropToPoint,
            "format" => $format,
        ],
        "3840w" => [
            "width" => intval(3840),
            "height" => intval(3840 / $imageRatioDesktop),
            "crop" => $imageCropToPoint,
            "format" => $format,
        ],
    ];
}

function getRatioCss($height, $width): string
{
    return "padding-bottom: " . number_format(($height / $width) * 100, 4, ".", "") . "%;";
}

function getRatioCssWithClass($imageClass, $src): string
{
    return "." . $imageClass . "{" . getRatioCss($src["height"], $src["width"]) . "}";
}

function getMaxWidthString($srcsetArr): string
{
    $itemIndex = 1;
    $maxWidth = "";
    foreach ($srcsetArr as $arrItem) {
        $maxWidth .= "(max-width:" . $arrItem["width"] . "px)" . $arrItem["width"] . "px";
        if ($itemIndex < count($srcsetArr)) {
            $maxWidth .= ", ";
        }
        $itemIndex++;
    }
    return $maxWidth;
}

function getRatioMultipleCss($srcsetArr, $imageClass): string
{
    $srcsetIndex = 0;
    $css = "";
    foreach (array_reverse($srcsetArr) as $src) {
        if ($srcsetIndex == 0) {
            $css .= "." . $imageClass . "{" . getRatioCss($src["height"], $src["width"]) . "}";
        } else {
            $css .=
                " @media only screen and (max-width:" .
                $src["width"] .
                "px) {" .
                "." .
                $imageClass .
                "{" .
                getRatioCss($src["height"], $src["width"]) .
                "}}";
        }
        $srcsetIndex++;
    }
    return $css;
}
